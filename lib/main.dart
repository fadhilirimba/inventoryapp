import 'package:flutter/material.dart';

import 'home_screen.dart';
import 'login_page.dart';
import 'sign_up.dart';
import 'product_list.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Inventoryapp',
        initialRoute: '/',
        routes: {
          '/': (context) => LoginPage(),
          '/first': (context) => SignupPage(),
         '/second': (context) => Homescreen(),
         // '/third': (context) => Drawer(),
         '/fourth': (context) => ProductList(),
         // '/fifthh': (context) => ProductForm(),




        }
        
        );
  }
}
