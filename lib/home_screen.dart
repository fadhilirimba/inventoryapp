
import 'package:flutter/material.dart';

class Homescreen extends StatefulWidget {
  @override
  _HomescreenState createState() => _HomescreenState();
}

class _HomescreenState extends State<Homescreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        
        
        
      appBar: AppBar(title: Text('Home',)),
     body: new Center(
       
      
          child: new Column(children: <Widget>[ new
            Text('welcome to point of sale',style: TextStyle(
                  fontSize: 30.0,
                  fontFamily: 'Roboto',
                  color: Colors.green[1000])),
            new RaisedButton(
              onPressed: () => {},
              child: new Text('Products'),
              color: Colors.green,

            ),
    
            new RaisedButton(
              onPressed: () => {},
              child: new Text('Sell'),
              color: Colors.green,
            ),
            new RaisedButton(
              onPressed: () => {},
              child: new Text('Customer'),
              color: Colors.green),
            new RaisedButton(
              onPressed: () => {},
              child: new Text('Transaction'),
              color: Colors.green,
            ),
            
            ]
            )
            ),
      

      
      drawer: Drawer(
      
        child: ListView(
          
          padding: EdgeInsets.zero,
          children: <Widget>[
            Container(
              child: Image.asset("images/downloaded.jpg"),
            ),
            DrawerHeader(
              child: Text('Sibasi P.O.S.'),
              decoration: BoxDecoration(
                color: Colors.blue,
            
              ),
            ),
            Divider(
              color: Colors.redAccent,
              height:5),
            ListTile(
              trailing: Icon(Icons.emoji_objects_rounded,color: Colors.redAccent,),
              title: Text('Product',style: TextStyle(color:Colors.black,fontSize: 18),),
              onTap: () {
               Navigator.pushNamed(context, '/fourth');
              },
            ),
              Divider(
              color: Colors.redAccent,
              height:5),
            ListTile(
              trailing: Icon(Icons.emoji_objects_rounded,color: Colors.redAccent,),
              title: Text('Sells',style: TextStyle(color:Colors.black,fontSize: 18),),
              onTap: () {
               Navigator.pushNamed(context, '/third');
              },
            ),
               Divider(
              color: Colors.redAccent,
              height:5),
            ListTile(
              trailing: Icon(Icons.emoji_objects_rounded,color: Colors.redAccent,),
              title: Text('Customer',style: TextStyle(color:Colors.black,fontSize: 18),),
              onTap: () {
               Navigator.pushNamed(context, '/third');
              },
            ),
             Divider(
              color: Colors.redAccent,
              height:5),
            ListTile(
              trailing: Icon(Icons.emoji_objects_rounded,color: Colors.redAccent,),
              title: Text('Transaction',style: TextStyle(color:Colors.black,fontSize: 18),),
              onTap: () {
               Navigator.pushNamed(context, '/third');
              },
            ),
              Divider(
              color: Colors.redAccent,
              height:5),
            ListTile(
              trailing: Icon(Icons.emoji_objects_rounded,color: Colors.redAccent,),
              title: Text('Supplier',style: TextStyle(color:Colors.black,fontSize: 18),),
              onTap: () {
               Navigator.pushNamed(context, '/third');
              },
            ),
              Divider(
              color: Colors.redAccent,
              height:5),
            ListTile(
              trailing: Icon(Icons.emoji_objects_rounded,color: Colors.redAccent,),
              title: Text('Profile',style: TextStyle(color:Colors.black,fontSize: 18),),
              onTap: () {
               Navigator.pushNamed(context, '/third');
              },
            ),
             Divider(
              color: Colors.white,
              height:5),
            ListTile(
              title: Text('Log out',style: TextStyle(color: Colors.redAccent,fontSize: 18)),
              trailing: Icon(Icons.logout,color: Colors.black87,),
              onTap: () {
                                Navigator.pushNamed(context, '/');

              
              },
            ),
        
           
        
          ],
        ),
      ),
    ));
  }
}

