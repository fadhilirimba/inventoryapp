import 'package:flutter/material.dart';

class SignupPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<SignupPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController  lastnameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  TextEditingController repeatpasswordController = TextEditingController();

 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('Sign up'),
        ),
        body: Padding(
            padding: EdgeInsets.all(10),
            child: ListView(
              children: <Widget>[
 
                
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    controller: nameController,
                    decoration: InputDecoration(
                      icon: const Icon(Icons.border_color),
                      border: OutlineInputBorder(),
                      labelText: 'First Name',
                    ),
                  ),
                ),
                 Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    controller: lastnameController,
                    decoration: InputDecoration(
                      icon: const Icon(Icons.border_color),
                      border: OutlineInputBorder(),
                      labelText: 'Last Name',
                    ),
                  ),
                ),
                 Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    controller: mobileController,
                    decoration: InputDecoration(
                      icon: const Icon(Icons.smartphone),
                      border: OutlineInputBorder(),
                      labelText: 'Mobile',
                    ),
                  ),
                ),
                 Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    controller: emailController,
                    decoration: InputDecoration(
                      icon: const Icon(Icons.mail),
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    obscureText: true,
                    controller: passwordController,
                    decoration: InputDecoration(
                      icon: const Icon(Icons.login),
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                    ),
                  ),
                ), 
                 Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    obscureText: true,
                    controller: repeatpasswordController,
                    decoration: InputDecoration(
                      icon: const Icon(Icons.login),
                      border: OutlineInputBorder(),
                      labelText: ' Repeat Password',
                    ),
                  ),
                ), 
                Container(
                    height: 50,
                  padding: EdgeInsets.fromLTRB(5, 5, 5, 0),

                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Colors.black,
                      child: Text('Submit'),
                      onPressed: () {
                        Navigator.pushNamed(context, '/fourth');
                      },

              
                      
                    )),
               
              ],
            )));
  }
}
